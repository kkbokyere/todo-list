import * as React from "react";
import {render as rtlRender } from '@testing-library/react'
import { MockedProvider } from "@apollo/client/testing";
import {getAllTasks, addTaskMutation} from "../queries/task";

let queryCalled = false;

export const mocks = [
    {
        request: {
            query: getAllTasks,
        },
        result: () => {
           if(queryCalled) {
               return ({
                   data: {
                       tasks: [{
                           "id": '1',
                           "name": "new task 1",
                           "isComplete": false
                       }]
                   },
               })
           } else {
               queryCalled = true;
               return ({
                   data: {
                       tasks: [{
                           "id": '1',
                           "name": "new task 1",
                           "isComplete": false
                       },{
                           "id": '2',
                           "name": "new task 2",
                           "isComplete": false
                       }]
                   },
               })
           }
        },
    },
    {
        request: {
            query: addTaskMutation,
            variables:{ name: 'new task 2', isComplete: false, userId: "12"},
        },
        newData: jest.fn(() => ({ data: {
                tasks: [{
                    "id": '2',
                    "name": "new task 2",
                    "isComplete": false,
                    "userId": "12"
                }]
            } })),
    },
];

function render(
    ui,
    {
        ...renderOptions
    } = {},
) {
    function Wrapper({children}) {
        return (
            <MockedProvider mocks={mocks}>
                {children}
            </MockedProvider>
        )
    }
    return rtlRender(ui, {wrapper: Wrapper, ...renderOptions})
}
// re-export everything
export * from '@testing-library/react'

// override render method
export { render }
