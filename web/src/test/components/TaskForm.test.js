import React from 'react';
import { render } from '../helper'
import TaskList from "../../components/TaskList";
import TaskForm from "../../components/TaskForm";
const setup = (props) => {
    return render(<TaskForm {...props}/>);
};
describe('TaskForm', () => {
    test('renders TaskForm component', () => {
        const {asFragment} = setup();
        expect(asFragment()).toMatchSnapshot();
    });
});
