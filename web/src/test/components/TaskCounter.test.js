import React from 'react';
import { render, screen } from '../helper'
import TaskForm from "../../components/TaskForm";
import TaskCounter from "../../components/TaskCounter";
const setup = (props) => {
    return render(<TaskCounter {...props}/>);
};
describe('TaskForm', () => {
    test('renders TaskForm component', async () => {
        const {asFragment} = setup({
            noOfRemainingTasks: 10,
            totalTasks: 20
        });
        const remainingTasksElement = await screen.findByText('10 remaining out of 20 tasks');

        expect(asFragment()).toMatchSnapshot();
    });
});
