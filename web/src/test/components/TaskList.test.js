import React from 'react';
import { render } from '../helper'
import TaskList from "../../components/TaskList";
const setup = (props) => {
    return render(<TaskList {...props}/>);
};
describe('TaskList', () => {
    test('renders empty TaskList component', () => {
        const {asFragment} = setup({ tasks: []});
        expect(asFragment()).toMatchSnapshot();
    });
    test('renders TaskList component', () => {
        const {asFragment} = setup({ tasks: [{
                id:'1',
                name:'some task'
            }]});
        expect(asFragment()).toMatchSnapshot();
    });
});
