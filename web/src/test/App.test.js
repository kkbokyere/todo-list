import React from 'react';
import { render, fireEvent, screen } from './helper'
import App from '../App';

const setup = () => {
  return render(<App/>);
};
describe('Initial App render', () => {
  test('renders connected app component', () => {
    const {asFragment} = setup();
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('App ACs', () => {
  test('Should render tasks list', async () => {
    setup();
    const taskItem = await screen.findByText('new task 1');
  });

  test('Should add new task to list an refetch', async () => {
    setup();
    const addButton = await screen.findByText('Add');
    const input = screen.getByTestId('task-input');
    fireEvent.change(input, { target: { value: 'new task 2' } });
    fireEvent.click(addButton);
  })

});
