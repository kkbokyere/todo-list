import React from "react";

import {useMutation, useQuery, useSubscription} from "@apollo/client";

import TaskListContainer from "./TaskListContainer";
import {addTaskMutation, getAllTasks, taskAddedSubscription} from "../../queries/task";

export default () => {
    const { loading, error, data } = useQuery(getAllTasks);

    // TODO: listen out for the subscription
    // const { taskAdded } = useSubscription(
    //     taskAddedSubscription
    // );
    const [addTask] = useMutation(addTaskMutation, {
        refetchQueries: [{ query: getAllTasks }]
    });
    return (<TaskListContainer data={data} error={error} loading={loading} addTask={addTask}/>)
}
