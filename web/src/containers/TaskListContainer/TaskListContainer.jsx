import React, {useEffect, useState} from 'react';
import TaskList from '@components/TaskList'
import TaskForm from '@components/TaskForm'
import TaskCounter from '@components/TaskCounter'
const TaskListContainer = ({addTask, loading, error, data}) => {
    const [isLoading, setLoading] = useState(false);
    const [tasks, setTasks] = useState([]);
    const [taskName, setTaskName] = useState("");
    const [noOfRemainingTasks, setRemainingTasks] = useState(0);

    const handleAddNewTasks = (e) => {
        e.preventDefault();
        if(taskName)
            addTask({
                variables: {
                    userId: '',
                    name:taskName,
                    isComplete: false
                }
            })
                .catch((error) => { console.log(error)});
    };

    useEffect(() => {
        const filterRemainingTasks = () => {
            return tasks.filter(({ isComplete }) => !isComplete).length
        };
        setRemainingTasks(filterRemainingTasks())
    }, [tasks]);

    useEffect(() => {
        if(!loading) {
            setTasks(data.tasks);
        }
        setLoading(loading)
    }, [loading, data]);

    if(error) {
        return <div>{error}</div>
    }

    if(isLoading) {
        return <div>Loading tasks</div>
    }
    return (
        <div>
            <div>
                <h2>
                    Tasks List
                </h2>
                <TaskForm handleOnSubmit={handleAddNewTasks} selectedVal={taskName} handleOnChange={setTaskName}/>
                <TaskCounter noOfRemainingTasks={noOfRemainingTasks} totalTasks={tasks.length}/>
                <TaskList tasks={tasks}/>
            </div>
        </div>
    );
};

export default TaskListContainer;
