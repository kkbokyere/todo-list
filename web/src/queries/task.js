import { gql } from '@apollo/client';
export const getAllTasks = gql`
    {
        tasks {
            name
            id
            isComplete
        }
    }
`;

export const addTaskMutation = gql`    
    mutation AddTask($name: String!, $userId: ID!, $isComplete: Boolean) {
        addTask(name: $name, userId: $userId, isComplete: $isComplete){
            name
            isComplete
            id
        }
    }
`;

export const taskAddedSubscription = gql`
    subscription TaskAdded {
        taskAdded {
            name
            id
        }
    }
`;
