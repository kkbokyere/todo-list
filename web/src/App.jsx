import React from 'react';
import Layout from "./components/Layout";
import TaskListContainer from "./containers/TaskListContainer";

function App() {
  return (
    <div className="App">
      <Layout>
        <TaskListContainer/>
      </Layout>
    </div>
  );
}

export default App;
