import React from "react";

const TaskForm = ({ handleOnSubmit, selectedVal, handleOnChange }) => {
    return (
        <form onSubmit={handleOnSubmit}>
            <input data-testid="task-input" name="task" type="text" value={selectedVal}
                   onChange={({ target: { value }}) => handleOnChange(value)}
            />
            <button>Add</button>
        </form>
    )
};

export default TaskForm;
