import React from "react";

const TaskCounter = ({noOfRemainingTasks, totalTasks}) => (
    <div className="task-counter">
        {noOfRemainingTasks} remaining out of {totalTasks} tasks
    </div>
);

export default TaskCounter;
