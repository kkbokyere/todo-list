import React from "react";

const TaskList = ({ tasks }) => {
    return (
        <ul>
            {tasks.map(({ id, name }) => {
                return <li key={id}>{name}</li>
            })}
        </ul>
    )
};
export default TaskList
