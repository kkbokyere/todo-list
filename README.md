# On The Move Test

## Introduction

The full stack code for the On The Move Todo List app. Built with:

### Front End Stack:
- [React](https://reactjs.org/)
- [Apollo](https://www.apollographql.com/docs/react/)
### Backend Stack
- [GraphQL](https://graphql.org/)
- [NodeJS](https://nodejs.org/en/)
- [Express](https://expressjs.com/)
- [MongoDB](https://mlab.com/) (Using mLab)

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone https://kkbokyere@bitbucket.org/kkbokyere/todo-list.git
```

**2. Install Front End Dependencies.**

Once that's all done, cd into the todo-list directory and install the depedencies:

```
$ cd web
$ yarn install
```

**3. Install Backend Dependencies.**

Once that's all done, cd into the todo-list directory and install the depedencies:

```
$ cd server
$ yarn install
```

**4. Run Back End Application.**

```
$ cd server
$ yarn start
```

**5. Run Front End Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. To open up a local development environment, run:

```
$ cd web
$ yarn start
```

Once the server is up and running, navigate to [localhost:3000](http://localhost:3000).

## Testing

[Jest](https://jestjs.io/) is the test runner used, with [React Testing Library](https://testing-library.com/docs/react-testing-library/) is testing library used for testing components. To run test use the following command:

```
$ yarn test
```

## Deployment

No CI/CD pipeline at the moment.

# Tools Used

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- [Redux](https://redux.js.org)
- [Redux Saga](https://github.com/redux-saga/redux-saga)
- [CSS Modules](https://github.com/css-modules/css-modules)
- [Webpack](https://webpack.js.org/)

# Improvements / Retrospective Review

- Would have used Cypress for E2E testing
- 100% test coverage
- I tried to use graphql subscription for realtime data syncing, but I was getting some errors with the WS connection.
- Originally, was going to use just a node/express api with a websocket connection, but it thought id give graphql a try.
- create a better Error handler
- implement typescript to better type definitions
- Would have considered creating a proper [SMACSS](http://smacss.com/) architecture for base CSS styles such as layout. 
- better styling
- better seperation in the server side code
