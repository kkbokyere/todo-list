const graphql = require('graphql');
const  socket = require('../socket');
const { withFilter } = require('graphql-subscriptions');

const {Task, User} = require('../models');
const { GraphQLObjectType,
    GraphQLString,
    GraphQLBoolean,
    GraphQLSchema,
    GraphQLID,
    GraphQLList,
    GraphQLNonNull
} = graphql;

const TaskType = new GraphQLObjectType({
   name: 'Task',
   fields: () => ({
       id: { type: GraphQLID },
       name: { type: GraphQLString },
       isComplete: { type: GraphQLBoolean },
       user: {
           type: UserType,
           resolve: (parent, args) => {
               return Task.findById(parent.userId)
           }
       }
   })
});

const UserType = new GraphQLObjectType({
   name: 'User',
   fields: () => ({
       id: { type: GraphQLID },
       name: { type: GraphQLString },
       tasks: {
           type: new GraphQLList(TaskType),
           resolve: (parent, args) => {
               return User.find({ userId: parent.id })
           }
       }
   })
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addTask:{
            type:TaskType,
            args:{
                name: { type: new GraphQLNonNull(GraphQLString) },
                userId: { type: new GraphQLNonNull(GraphQLID) },
                isComplete: { type: GraphQLBoolean }
            },
            resolve: (parent, { name, isComplete = false, userId }) => {
                let task = new Task({
                    name,
                    isComplete,
                    userId
                });
                return task.save().then((data) => {
                    socket.publish('TASK_ADDED', {
                        taskAdded: data
                    })
                })
            }
        },
        addUser: {
            type: UserType,
            args: {
                name: { type: GraphQLString }
            },
            resolve: (parent, { name }) => {
                let user = new User({
                    name
                });
                return user.save()
            }
        }
    }
});

const Subscription = new GraphQLObjectType({
    name: 'Subscription',
    fields: {
        taskAdded: {
            type: TaskType,
            subscribe: () => socket.asyncIterator('TASK_ADDED')
        }
    }
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        task: {
            type: TaskType,
            args: { id: { type: GraphQLID } },
            resolve: (parent, args) => Task.findById(args.id)
        },
        user: {
            type: UserType,
            args: { id: { type: GraphQLID } },
            resolve: (parent, args) => User.findById(args.id)
        },
        tasks: {
            type: new GraphQLList(TaskType),
            resolve: () => Task.find({})
        },
        users: {
            type: new GraphQLList(UserType),
            resolve: () => User.find({})
        },
    }
});

module.exports = new GraphQLSchema({
   query: RootQuery,
    mutation: Mutation,
    subscription: Subscription
});
