const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { createServer } = require('http');
const { graphqlExpress, graphiqlExpress } = require('graphql-server-express');
const { SubscriptionServer } = require('subscriptions-transport-ws');
const { subscribe, execute } = require('graphql');
const graphqlSchema = require('./schema/schema');
const mongoose = require('mongoose');

const app = express();
const PORT = 4000;
//Connect to mongo db

mongoose.connect('mongodb+srv://admin:testadmin@gql-otm.eznsl.mongodb.net/gql-otm?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
mongoose.connection.once('open', () => {
    console.log('connected to DB')
});

app.use(bodyParser.json());

// enable `cors` to set HTTP response header: Access-Control-Allow-Origin: *
app.use(cors());

// Create middleware for express graph
app.use('/graphql', graphqlExpress({
    schema: graphqlSchema
}));

app.use('/graphiql', graphiqlExpress({
    endpointURL: 'graphql',
    subscriptionsEndpoint: `ws://localhost:${PORT}/subscriptions`
}));

const server = createServer(app);

app.listen(PORT, () => {
    new SubscriptionServer({
        schema: graphqlSchema,
        execute,
        subscribe,
        onConnect: () => { return console.log('subscription client connected')}
    }, {
        server,
        path: '/subscriptions'
    });
    console.log('now listening on port 4000')

});

